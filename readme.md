Proxy XMPP

Para compilar el proxy nos debemos situar en la carpeta principal y ejecutar el comando: 'mvn package'.
Luego, para poder ejecutar el archivo .jar generado, debemos contener en el mismo directorio el archivo 'config.properties'. El archivo .jar a utilizar es el de nombre 'TP_PDC-0.0.1-SNAPSHOT-jar-with-dependencies.jar'.'
Finalmente, para correr el archivo .jar debemos ejecutar el comando 'java -jar TP_PDC-0.0.1-SNAPSHOT-jar-with-dependencies.jar'.

Funcionalidades:
- Multiplexado
- Silenciado de usuarios
- Conversion L33t
- Administracion remota
- Metricas

Guia de instalacion rapida:
En primer lugar, necesitaremos un cliente para el proxy y un servidor con el cual se pueda comunicar. Para esto, proponemos como cliente a Pidgin y como servidor a Ejabberd, por su fácil configuración y manipulación.
	Una vez descargadas e instaladas dichas herramientas, reemplazamos el archivo de configuración de ejabberd (ejabberd.cfg) por el que se encuentra en el repositorio de la materia (https://bitbucket.org/itba/cg-tp-2015-01) ya que este especifica que el mismo se comporte sin encriptacion.
	Paso siguiente, procedemos a la creación de una cuenta en nuestro servidor. Lo podemos obtener desde consola mediante el comando:
‘ejabberdctl register username domain password’.
	Una vez creado el usuario en el servidor, lo agregaremos como usuario de nuestro cliente (Pidgin), configurando en la solapa de ‘connection’ la ip y el puerto en donde se encuentra montado el proxy. Para cambiar el puerto en el que se monta el proxy habrá que volver a compilar el proyecto cambiando el archivo de propiedades en donde se encuentra. También debemos seleccionar la opción para que no se utilice encriptación para autentificarse y si se haga de manera plana.
	Ahora si, nos encontramos en condiciones de correr el proxy.
	Para configurar el default server al que se conectarán los clientes, o multiplexarlos, iniciamos una conexión netcat en el puerto 9091 (se puede modificar de la misma manera que el puerto en el que corre el proxy): de la siguiente manera: ‘nc ip 9091’.
	Asegurándonos de que el ejabberd se encuentra corriendo, iniciamos sesión con nuestro cliente en Pidgin. Deberíamos encontrar a nuestro cliente ‘en línea’. Se pueden seguir configurando clientes tal como se lo hizo con el cliente de ejemplo.
	Si se desea modificar variables de del proxy, tal como el leet converter, el silenciado de usuarios, etc., se tiene la posibilidad de proceder cómo se lo indica en la sección 1.4
