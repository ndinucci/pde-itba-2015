package ar.edu.itba.pdc.tcp.protocols.admin.command;

import java.util.regex.Pattern;

import ar.edu.itba.pdc.tcp.properties.PropertieManager;

public class AdminCommands {
	// TODO: Pasar a un archivo.
	public static final String ADMIN_USER = PropertieManager.getPropertie("adminUser");
	public static final String ADMIN_PASSWORD = PropertieManager.getPropertie("adminPassword");

	/* WELCOME MESSAGE */
	public static final String WELCOME_MSG = "admin protocol:\n" + "(you can use help)\r\n";

	/* HELP DESCRIPTIONS */
	private static final String USER_HELP = "- user {user}@{password}\n";
	private static final String ACCESSES_HELP = "- accesses\n";
	private static final String BLOCKED_MESSAGES_HELP = "- blocked messages\n";
	private static final String BYTES_TRANSFERRED_HELP = "- bytes transferred\n";
	private static final String BYTES_RECEIVED_HELP = "- bytes received\n";
	private static final String MULTIPLEX_HELP = "- multiplex {user} {host}:{port}\n";
	private static final String DEMULTIPLEX_HELP = "- demultiplex {user}\n";
	private static final String DEFAULT_SERVER_HELP = "- defaultserver {host}:{port}\n";
	private static final String MUTE_HELP = "- mute {user}\n";
	private static final String UNMUTE_HELP = "- unmute {user}\n";
	private static final String LEET_ON_HELP = "- leet on\n";
	private static final String LEET_OFF_HELP = "- leet off\n";
	private static final String QUIT_HELP = "- quit\r\n";

	/* ANSWERS */
	public static final String CORRECT_LOG_IN = "ok, welcome %s!\n";
	public static final String INCORRECT_LOGIN = "error, invalid user or password\n";
	public static final String CORRECT_ACCESSES = "ok, %d times\n";
	public static final String CORRECT_BLOCKED_MESSAGES = "ok, %d blocked messages\n";
	public static final String CORRECT_BYTES_TRANSFERRED = "ok, %d bytes transferred\n";
	public static final String CORRECT_BYTES_RECEIVED = "ok, %d bytes received\n";
	public static final String CORRECT_MULTIPLEX = "ok, %s now connects to %s (port %s)\n";
	public static final String CORRECT_DEMULTIPLEX = "ok, %s now connects to %s (port %s)\n";
	public static final String CORRECT_DEFAULT_SERVER = "ok, %s (port %s) now is default server\n";
	public static final String CORRECT_MUTE = "ok, %s is muted\n";
	public static final String CORRECT_UNMUTE = "ok, %s is unmuted\n";
	public static final String CORRECT_LEET_ON = "ok, leet now is on\n";
	public static final String CORRECT_LEET_OFF = "ok, leet now is off\n";
	public static final String INVALID_COMMAND = "error, invalid command\n";
	public static final String CORRECT_QUIT = "ok, bye!\n";
	public static final String INVALID_LENGTH = "error, invalid length\n";
	public static final String INVALID_STATE_LOGGED_IN = "error, currently logged\n";
	public static final String INVALID_STATE_NORMAL = "error, currently not logged\n";
	public static final String INVALID_USER_FORMAT = "error, invalid user\n";
	public static final String INVALID_PORT = "error, invalid port\n";
	public static final String CORRECT_HELP = "ok,\n" + USER_HELP + ACCESSES_HELP
			+ BLOCKED_MESSAGES_HELP + BYTES_TRANSFERRED_HELP + BYTES_RECEIVED_HELP + MULTIPLEX_HELP
			+ DEMULTIPLEX_HELP + DEFAULT_SERVER_HELP + MUTE_HELP + UNMUTE_HELP + LEET_ON_HELP
			+ LEET_OFF_HELP + QUIT_HELP;

	/* COMMANDS */
	public static final Pattern USER_COMMAND = Pattern.compile("user \\S+@\\S+");
	public static final Pattern ACCESSES_COMMAND = Pattern.compile("accesses");
	public static final Pattern BLOCKED_MESSAGES_COMMAND = Pattern.compile("blocked messages");
	public static final Pattern BYTES_TRANSFERRED_COMMAND = Pattern.compile("bytes transferred");
	public static final Pattern BYTES_RECEIVED_COMMAND = Pattern.compile("bytes received");
	public static final Pattern MULTIPLEX_COMMAND = Pattern
			.compile("multiplex \\S+ \\S+:((0)|([1-9][0-9]*))");
	public static final Pattern DEMULTIPLEX_COMMAND = Pattern.compile("demultiplex \\S+");
	public static final Pattern DEFAULT_SERVER_COMMAND = Pattern
			.compile("defaultserver \\S+:((0)|([1-9][0-9]*))");
	public static final Pattern UNMUTE_COMMAND = Pattern.compile("unmute \\S+");
	public static final Pattern MUTE_COMMAND = Pattern.compile("mute \\S+");
	public static final Pattern LEET_ON_COMMAND = Pattern.compile("leet on");
	public static final Pattern LEET_OFF_COMMAND = Pattern.compile("leet off");
	public static final Pattern QUIT_COMMAND = Pattern.compile("quit");
	public static final Pattern HELP_COMMAND = Pattern.compile("help");
}