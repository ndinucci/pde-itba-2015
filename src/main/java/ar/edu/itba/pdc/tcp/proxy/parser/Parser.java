package ar.edu.itba.pdc.tcp.proxy.parser;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.aalto.AsyncByteBufferFeeder;
import com.fasterxml.aalto.AsyncXMLInputFactory;
import com.fasterxml.aalto.AsyncXMLStreamReader;
import com.fasterxml.aalto.stax.InputFactoryImpl;

public class Parser {

	private static AsyncXMLInputFactory factory = new InputFactoryImpl();
	private ByteBuffer buffer;
	private AsyncXMLStreamReader<AsyncByteBufferFeeder> parser;
//	private List<String> tagNames = new ArrayList<String>();

	public Parser(ByteBuffer buffer) {
		this.buffer = buffer;
		this.parser = factory.createAsyncForByteBuffer();
	}

	void parse() {
		try {
			buffer.flip();
			parser.getInputFeeder().feedInput(buffer);
			int type,i;
			while ((type = parser.next()) != AsyncXMLStreamReader.EVENT_INCOMPLETE) {
				switch (type) {
				case AsyncXMLStreamReader.START_ELEMENT: {
					System.out.println("arranco  " + parser.getLocalName());
					System.out.println(parser.getAttributeValue(null, "va2lue"));
					i=parser.getAttributeCount();
					for(int j=0;j<i;j++){
						System.out.println(parser.getAttributeLocalName(j));
					}
					break;
				}
				case AsyncXMLStreamReader.END_ELEMENT: {
					System.out.println("termino  " + parser.getLocalName());
					break;
				}
				case AsyncXMLStreamReader.CHARACTERS: {
					System.out.println("charas   " + parser.getText());
					break;
				}

				}
			}
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		
//		ByteBuffer bufferA = ByteBuffer.allocate(256);
//		bufferA.put("<stream:stream to='localhost' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0'>h' ga:client-uses-full"
//				.getBytes());
//		ByteBuffer bufferB = ByteBuffer.wrap("a</message></xml>".getBytes());
//		Parser par = new Parser(bufferA);
//		par.parse();
//		//bufferA.put(bufferB);
//		//par.parse();
		try {
			String toEncode = "\0nicolas\0"+"123123\0";
			String encoded = Base64.encodeBase64String(toEncode.getBytes());
			System.out.println(new String(Base64.decodeBase64(encoded) ,"UTF-8").split("\0")[1]);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
