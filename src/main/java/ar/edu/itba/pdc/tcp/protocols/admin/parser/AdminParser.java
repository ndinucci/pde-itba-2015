package ar.edu.itba.pdc.tcp.protocols.admin.parser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import ar.edu.itba.pdc.tcp.protocols.admin.command.AdminCommands;
import ar.edu.itba.pdc.tcp.protocols.admin.command.AdminCommandsState;
import ar.edu.itba.pdc.tcp.proxy.XMPPProxyConfiguration;
import ar.edu.itba.pdc.tcp.proxy.metrics.MetricsService;
import ar.edu.itba.pdc.tcp.proxy.router.Address;

enum AdminState {
	NORMAL, LOGGED_IN;
}

public class AdminParser {
	private AdminState state = AdminState.NORMAL;
	private static final int minimumPort = 0;
	private static final int maximumPort = 65535;
	private static final int BUFFER_SIZE = 512;
	private ByteBuffer clientBuffer = ByteBuffer.allocate(BUFFER_SIZE);
	private ByteBuffer serverBuffer = ByteBuffer.allocate(BUFFER_SIZE);
	private String line = "";
	private boolean isLineDone = false;
	private boolean isBufferFull = false;
	private SocketChannel clientChannel;
	private boolean isConnected = true;

	public AdminParser(SocketChannel clientChannel) throws IOException {
		this.clientChannel = clientChannel;
		clientBuffer.put(AdminCommands.WELCOME_MSG.getBytes());
		clientBuffer.flip();
		clientChannel.write(clientBuffer);
		clientBuffer.clear();
	}

	public long read() throws IOException {
		long bytesRead = clientChannel.read(clientBuffer);
		readLine();
		if (isBufferFull) {
			// TODO: Ver si lo puedo mejorar, no deberia estar acá.
			clientChannel.write(ByteBuffer.wrap(AdminCommands.INVALID_LENGTH.getBytes()));
			clearLine();
		}
		return bytesRead;
	}

	public ByteBuffer getServerBuffer() {
		if (isLineDone) {
			line = line.replace("\n", "").replace("\r", "").replace("\t", "").replaceAll(" +", " ")
					.trim();
			if (line.length() != 0) {
				String answer = process();
				serverBuffer.put(answer.getBytes());
			}
			resetLine();
		}
		return serverBuffer;
	}

	private String process() {
		String answer = "";
		AdminCommandsState command = getCommand();
		switch (state) {
		case NORMAL:
			answer = normalState(command);
			break;
		case LOGGED_IN:
			answer = loggedInState(command);
			break;
		}
		return answer;
	}

	public boolean isLineDone() {
		return isLineDone;
	}

	private void readLine() {
		clientBuffer.flip();
		if (clientBuffer.limit() == 1) {
			clientBuffer.clear();
			return;
		}
		line = new String(clientBuffer.array()).substring(0, clientBuffer.limit()).split("\n")[0];
		if (line.length() >= BUFFER_SIZE) {
			isBufferFull = true;
		} else {
			isLineDone = true;
		}
		clientBuffer.clear();
	}

	private void resetLine() {
		isLineDone = false;
		isBufferFull = false;
		line = "";
	}

	private void clearLine() throws IOException {
		while (clientChannel.read(clientBuffer) > 0) {
			clientBuffer.clear();
		}
		resetLine();
	}

	private boolean checkUser(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException e) {
			result = false;
		}
		return result;
	}

	private boolean checkPort(String port) {
		int portValue = -1;
		try {
			portValue = Integer.valueOf(port);
		} catch (NumberFormatException e) {
			return false;
		}
		return portValue >= minimumPort && portValue <= maximumPort;
	}

	private AdminCommandsState getCommand() {
		if (matchPattern(AdminCommands.USER_COMMAND)) {
			return AdminCommandsState.USER;
		}
		if (matchPattern(AdminCommands.ACCESSES_COMMAND)) {
			return AdminCommandsState.ACCESSES;
		}
		if (matchPattern(AdminCommands.BLOCKED_MESSAGES_COMMAND)) {
			return AdminCommandsState.BLOCKED_MESSAGES;
		}
		if (matchPattern(AdminCommands.BYTES_TRANSFERRED_COMMAND)) {
			return AdminCommandsState.BYTES_TRANSFERRED;
		}
		if (matchPattern(AdminCommands.BYTES_RECEIVED_COMMAND)) {
			return AdminCommandsState.BYTES_RECEIVED;
		}
		if (matchPattern(AdminCommands.MULTIPLEX_COMMAND)) {
			return AdminCommandsState.MULTIPLEX;
		}
		if (matchPattern(AdminCommands.DEMULTIPLEX_COMMAND)) {
			return AdminCommandsState.DEMULTIPLEX;
		}
		if (matchPattern(AdminCommands.DEFAULT_SERVER_COMMAND)) {
			return AdminCommandsState.DEFAULT_SERVER;
		}
		if (matchPattern(AdminCommands.MUTE_COMMAND)) {
			return AdminCommandsState.MUTE;
		}
		if (matchPattern(AdminCommands.UNMUTE_COMMAND)) {
			return AdminCommandsState.UNMUTE;
		}
		if (matchPattern(AdminCommands.LEET_ON_COMMAND)) {
			return AdminCommandsState.LEET_ON;
		}
		if (matchPattern(AdminCommands.LEET_OFF_COMMAND)) {
			return AdminCommandsState.LEET_OFF;
		}
		if (matchPattern(AdminCommands.QUIT_COMMAND)) {
			return AdminCommandsState.QUIT;
		}
		if (matchPattern(AdminCommands.HELP_COMMAND)) {
			return AdminCommandsState.HELP;
		}
		return AdminCommandsState.ERROR;
	}

	private boolean matchPattern(Pattern pattern) {
		return pattern.matcher(line).matches();
	}

	private String normalState(AdminCommandsState command) {
		String answer = "";
		switch (command) {
		case ERROR:
			answer = AdminCommands.INVALID_COMMAND;
			break;
		case HELP:
			answer = AdminCommands.CORRECT_HELP;
			break;
		case USER:
			if (AdminCommands.USER_COMMAND.matcher(line).matches()) {
				String[] userAndPassword = line.split(" ")[1].split("@");
				String user = userAndPassword[0].trim();
				String password = userAndPassword[1].trim();
				if (user.equals(AdminCommands.ADMIN_USER) && password.equals(AdminCommands.ADMIN_PASSWORD)) {
					answer = String.format(AdminCommands.CORRECT_LOG_IN, user);
					state = AdminState.LOGGED_IN;
				} else {
					answer = AdminCommands.INCORRECT_LOGIN;
				}
			}
			break;
		case QUIT:
			isConnected = false;
			answer = AdminCommands.CORRECT_QUIT;
			break;
		case ACCESSES:
		case BLOCKED_MESSAGES:
		case BYTES_TRANSFERRED:
		case BYTES_RECEIVED:
		case MULTIPLEX:
		case DEMULTIPLEX:
		case DEFAULT_SERVER:
		case MUTE:
		case UNMUTE:
		case LEET_ON:
		case LEET_OFF:
			answer = AdminCommands.INVALID_STATE_NORMAL;
			break;
		}
		return answer;
	}

	private String loggedInState(AdminCommandsState command) {
		XMPPProxyConfiguration proxyConfiguration = XMPPProxyConfiguration.getInstance();
		MetricsService metrics = MetricsService.getInstance();
		String answer = "";
		switch (command) {
		case ERROR:
			answer = AdminCommands.INVALID_COMMAND;
			break;
		case HELP:
			answer = AdminCommands.CORRECT_HELP;
			break;
		case ACCESSES:
			long accesses = metrics.getAccess();
			answer = String.format(AdminCommands.CORRECT_ACCESSES, accesses);
			break;
		case BLOCKED_MESSAGES:
			long blockedMessages = metrics.getBlockedMessages();
			answer = String.format(AdminCommands.CORRECT_BLOCKED_MESSAGES, blockedMessages);
			break;
		case BYTES_TRANSFERRED:
			long bytesTransferred = metrics.getTransferedBytes();
			answer = String.format(AdminCommands.CORRECT_BYTES_TRANSFERRED, bytesTransferred);
			break;
		case BYTES_RECEIVED:
			long bytesReceived = metrics.getReceivedBytes();
			answer = String.format(AdminCommands.CORRECT_BYTES_RECEIVED, bytesReceived);
			break;
		case MULTIPLEX: {
			String[] lineSplited = line.split(" ");
			String user = lineSplited[1].trim();
			String[] serverPort = lineSplited[2].split(":");
			String server = serverPort[0].trim();
			String port = serverPort[1].trim();
			if (!checkUser(user)) {
				answer = AdminCommands.INVALID_USER_FORMAT;
			} else if (!checkPort(port)) {
				answer = AdminCommands.INVALID_PORT;
			} else {
				proxyConfiguration.addSpecificRouteForUser(user, server, port);
				answer = String.format(AdminCommands.CORRECT_MULTIPLEX, user, server, port);
			}
		}
			break;
		case DEMULTIPLEX: {
			String user = line.split(" ")[1].trim();
			if (!checkUser(user)) {
				answer = AdminCommands.INVALID_USER_FORMAT;
			} else {
				proxyConfiguration.deleteSpecificRouteForUser(user);
				Address address = proxyConfiguration.getRouteForUser(user);
				answer = String.format(AdminCommands.CORRECT_DEMULTIPLEX, user, address.getRoute(), address.getPort());
			}
		}
			break;
		case DEFAULT_SERVER: {
			String[] serverPort = line.split(" ")[1].split(":");
			String server = serverPort[0].trim();
			String port = serverPort[1].trim();
			if (!checkPort(port)) {
				answer = AdminCommands.INVALID_PORT;
			} else {
				proxyConfiguration.setDefaultRoute(server, port);
				answer = String.format(AdminCommands.CORRECT_DEFAULT_SERVER, server, port);
			}
		}
			break;
		case MUTE: {
			String user = line.split(" ")[1].trim();
			if (!checkUser(user)) {
				answer = AdminCommands.INVALID_USER_FORMAT;
			} else {
				proxyConfiguration.muteUser(user);
				answer = String.format(AdminCommands.CORRECT_MUTE, user);
			}
		}
			break;
		case UNMUTE: {
			String user = line.split(" ")[1].trim();
			if (!checkUser(user)) {
				answer = AdminCommands.INVALID_USER_FORMAT;
			} else {
				proxyConfiguration.unmuteUser(user);
				answer = String.format(AdminCommands.CORRECT_UNMUTE, user);
			}
		}
			break;
		case LEET_ON:
			proxyConfiguration.setL33tConverter(true);
			answer = AdminCommands.CORRECT_LEET_ON;
			break;
		case LEET_OFF:
			proxyConfiguration.setL33tConverter(false);
			answer = AdminCommands.CORRECT_LEET_OFF;
			break;
		case QUIT:
			isConnected = false;
			answer = AdminCommands.CORRECT_QUIT;
			break;
		case USER:
			answer = AdminCommands.INVALID_STATE_LOGGED_IN;
			break;
		}
		return answer;
	}

	public boolean isConnected() {
		return isConnected;
	}
}
