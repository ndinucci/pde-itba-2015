package ar.edu.itba.pdc.tcp.proxy;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ar.edu.itba.pdc.tcp.properties.PropertieManager;
import ar.edu.itba.pdc.tcp.protocols.TCPProtocol;
import ar.edu.itba.pdc.tcp.protocols.admin.AdminProtocol;
import ar.edu.itba.pdc.tcp.protocols.xmpp.XMPPProxySelectorProtocol;

public class XMPPProxy {
	private static final TCPProtocol XMPProtocol = new XMPPProxySelectorProtocol();
	private static final AdminProtocol adminProtocol = new AdminProtocol();
	private static int adminProtocolPort;
	// TODO: borrar esta linea si no se usa.
	private static final Logger log = Logger.getLogger(XMPPProxy.class.getName());

	public static void main(String[] args) throws IOException {
		int proxyPort = getPort("proxyPort");
		int adminPort = getPort("adminPort");
		adminProtocolPort = adminPort;
		if (proxyPort == -1 || adminPort == -1 || proxyPort == adminPort)
			return;
		Selector selector = Selector.open();
		initializeServer(selector, proxyPort, adminPort);
		while (true) {
			selector.select(); // ¿Esta bien bloquearse acá?
			handleClient(selector.selectedKeys());
		}
	}

	private static int getPort(String propertie) {
		try {
			return Integer.valueOf(PropertieManager.getPropertie(propertie));
		} catch (NumberFormatException e) {
			log.error("invalid " + propertie + " propertie");
			return -1;
		}
	}

	private static void initializeServer(Selector selector, int proxyPort, int adminPort)
			throws IOException {
		initializeChannel(selector, proxyPort); // XMMP Proxy Channel
		initializeChannel(selector, adminPort); // Admin Channel
	}

	private static void initializeChannel(Selector selector, int port) throws IOException {
		ServerSocketChannel listenChannel = ServerSocketChannel.open();
		InetSocketAddress address = new InetSocketAddress(port);
		listenChannel.socket().bind(address);
		listenChannel.configureBlocking(false);
		listenChannel.register(selector, SelectionKey.OP_ACCEPT);
	}

	private static void handleClient(Set<SelectionKey> keys) throws IOException {
		Iterator<SelectionKey> keyIterator = keys.iterator();
		while (keyIterator.hasNext()) {
			SelectionKey key = keyIterator.next();
			TCPProtocol protocol = getProtocol(key.channel());
			if (protocol != null) {
				if (key.isAcceptable()) {
					protocol.handleAccept(key);
				}
				if (key.isReadable()) {
					protocol.handleRead(key);
				}
				if (key.isValid() && key.isWritable()) {
					protocol.handleWrite(key);
				}
			}
			keyIterator.remove();
		}
	}

	private static TCPProtocol getProtocol(SelectableChannel channel) throws IOException {
		if (!channel.isOpen()) {
			return null;
		}
		if (checkChannelPort(channel, adminProtocolPort)) {
			return adminProtocol;
		}
		return XMPProtocol;
	}

	private static boolean checkChannelPort(SelectableChannel channel, int port) throws IOException {
		try {
			SocketChannel socketChannel = (SocketChannel) channel;
			InetSocketAddress address = (InetSocketAddress) socketChannel.getLocalAddress();
			int channelPort = address.getPort();
			return channelPort == adminProtocolPort;
		} catch (ClassCastException e) {
			ServerSocketChannel socketChannel = (ServerSocketChannel) channel;
			InetSocketAddress address = (InetSocketAddress) socketChannel.getLocalAddress();
			int channelPort = address.getPort();
			return channelPort == adminProtocolPort;
		}
	}
}
